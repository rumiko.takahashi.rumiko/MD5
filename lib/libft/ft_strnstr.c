/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 20:07:21 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/19 03:11:54 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t len)
{
	size_t	i;
	size_t	j;
	size_t	temp;

	i = 0;
	if (s2[i] == '\0')
		return ((char *)s1);
	while (s1[i] && i < len)
	{
		j = 0;
		temp = i;
		while (s1[i] == s2[j] && i < len)
		{
			i++;
			j++;
			if (!s2[j])
				return ((char *)&s1[temp]);
		}
		i = temp;
		i++;
	}
	return (0);
}
