/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 12:05:44 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/01 19:22:04 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

uint32_t h0, h1, h2, h3;

void    md5(uint8_t *initial_msg, size_t initial_len)
{
    uint8_t *msg = NULL;
    int offset;
    int new_len;
    uint32_t bits_len;
    uint32_t i;
    uint32_t temp;
    uint32_t f, g;

    i = 0;
    offset = 0;
    h0 = 0x67452301;
    h1 = 0xefcdab89;
    h2 = 0x98badcfe;
    h3 = 0x10325476;
    
    new_len =initial_len * 8;
    while (new_len % 512 != 448)
        new_len++;
    new_len /= 8;
    msg = malloc((new_len + 64) * sizeof(uint32_t));
    ft_memcpy(msg, initial_msg, initial_len);
    msg[initial_len] = 128;
    bits_len = 8 * initial_len;
    ft_memcpy(msg + new_len, &bits_len, 4);
    while (offset < new_len)
    {
        uint32_t *w = (uint32_t *)(msg + offset);
        uint32_t a = h0;
        uint32_t b = h1;
        uint32_t c = h2;
        uint32_t d = h3;
        while (i < 64)
        {
            if (i < 16)
            {
                f = (b & c) | ((~b) & d);
                g = i;
            }
            else if (i < 32)
            {
                f = (d & b) | ((~d) & c);
                g = (5*i + 1) % 16;
            }
            else if (i < 48)
            {
                f = b ^ c ^ d;
                g = (3*i + 5) % 16;
            }
            else
            {
                f = c ^ (b | (~d));
                g = (7*i) % 16;
            }
            temp = d;
            d = c;
            c = b;
            b = b + LEFTROTATE((a + f + g_k[i] + w[g]), g_r[i]);
            a = temp;
            i++;
        }
        h0 += a;
        h1 += b;
        h2 += c;
        h3 += d;
        offset += 64;
    }
    free(msg);
}

int main(int argc, char **argv)
{
    uint8_t *p;
    size_t len;
    char *msg;

    if (argc < 2) {
        printf("usage: %s 'string'\n", argv[0]);
        return 1;
    }
    msg = argv[1];
    len = ft_strlen(msg);
    md5((uint8_t *)msg, len);
    p=(uint8_t *)&h0;
    printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3], h0);
    p=(uint8_t *)&h1;
    printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3], h1);
    p=(uint8_t *)&h2;
    printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3], h2);
    p=(uint8_t *)&h3;
    printf("%2.2x%2.2x%2.2x%2.2x", p[0], p[1], p[2], p[3], h3);
    puts("");
    // system("leaks a.out");
    return (0);
}
